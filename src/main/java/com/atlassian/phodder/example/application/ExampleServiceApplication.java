package com.atlassian.phodder.example.application;

import com.atlassian.phodder.example.application.configuration.ExampleServiceConfiguration;
import com.atlassian.phodder.example.application.dao.ExampleDao;
import com.atlassian.phodder.example.application.resource.ExampleResource;
import com.atlassian.phodder.example.application.service.ExampleService;
import com.atlassian.phodder.example.application.service.ExampleServiceImpl;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Strings;
import io.dropwizard.Application;
import io.dropwizard.configuration.UrlConfigurationSourceProvider;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

public class ExampleServiceApplication extends Application<ExampleServiceConfiguration> {
    private static final String DROPWIZARD_SERVER_ARGUMENT = "server";

    /**
     * Entry point for the application.
     * @param arguments the arguments provided by the callee
     * @throws Exception if anything goes wrong
     */
    public static void main(final String[] arguments) throws Exception {
        final String configFile = (arguments.length == 1) ? arguments[0] : System.getenv("MONITOR_SERVICE_CONFIG_FILE");

        if (Strings.isNullOrEmpty(configFile)) {
            throw new IllegalArgumentException("Invalid number of arguments provided. Should have been {dev | prod | test}.yml");
        }

        new ExampleServiceApplication().run(DROPWIZARD_SERVER_ARGUMENT, ClassLoader.getSystemResource(configFile).toString());
    }

    @Override
    public void initialize(final Bootstrap<ExampleServiceConfiguration> bootstrap) {

        bootstrap.setConfigurationSourceProvider(new UrlConfigurationSourceProvider());
        bootstrap.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
        bootstrap.getObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Override
    public void run(final ExampleServiceConfiguration configuration, final Environment environment) throws Exception {
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDatabase(), "postgresql");
        final ExampleDao exampleDao = jdbi.onDemand(ExampleDao.class);

        final ExampleService exampleService = new ExampleServiceImpl(exampleDao);

        final ExampleResource exampleResource = new ExampleResource(exampleService);
        environment.jersey().register(exampleResource);
    }
}
